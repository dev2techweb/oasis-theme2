<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package storefront
 */

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php storefront_homepage_content_styles(); ?>"
	data-featured-image="<?php echo esc_url( $featured_image ); ?>">
	<div class="col-full">
		<?php
		/**
		 * Functions hooked in to storefront_page add_action
		 *
		 * @hooked storefront_homepage_header      - 10
		 * @hooked storefront_page_content         - 20
		 */
		// do_action( 'storefront_homepage' );
		?>
	</div>
</div><!-- #post-## -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

	  
<section id="inicioz">

</section>

<div class="contenido">
	<section id="inicio">
	<div class="title__section">
        <h2>Quienes Somos</h2>
    </div>
    <div class="nm">
        <div class="wi">
            <h5>This is Photoshop’s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt. This is Photoshop’s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt. This is Photoshop’s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt.
            </h5>		  	
        </div>
    </div>
<section id="collage">

</section>

<section id="promociones">

</section>

	<div class="full-height d-flex a-center j-center" id="s2">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/productos (2).png" alt="">
    </div>
    

	<div id="informacion2">
        <h1>Galería</h1>
    </div>
	</section>
<section id="galeria" class="caja-res2">
    <article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 1.png" alt="">
            </figure>
        </a>
    </article>
    <article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 5.png" alt="">
            </figure>
        </a>
    </article>
    <article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 2.png" alt="">

            </figure>
        </a>
    </article>
    <article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 6.png" alt="">
            </figure>
        </a>
    </article>
	<article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 3.png" alt="">
            </figure>
        </a>
    </article>
    <article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 7.png" alt="">
            </figure>
        </a>
    </article>
    <article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 4.png" alt="">

            </figure>
        </a>
    </article>
    <article>
        <a target="_blank" href="#">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria 8.png" alt="">
            </figure>
        </a>
    </article>
</section>
<section id="abajo">

</section>
<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);
function ayuda(){
	echo "
	<a class=\"ayuda\" href=\"\">AYUDA</a>";
}

add_action( 'storefront_header', 'ayuda', 60 );
// add_action( 'storefront_header', 'storefront_header_cart',    60 );
add_action( 'storefront_header', 'storefront_product_search', 60 );

add_action( 'storefront_header', 'the_custom_logo', 50 );


require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */
 // personalizacion
// Scripts & Styles
function smart_styles () {
	// wp_register_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.1.1' );
	wp_register_style('icons', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '5.2.0');
	wp_register_style('icons2', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0');
	wp_register_style('style', get_template_directory_uri() . '/assets/css/tech.css', array(), '1.2.3');
	wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400', array('style'), '1.0.0');
	// wp_enqueue_style('bootstrap');
	wp_enqueue_style('icons');
	wp_enqueue_style('icons2');
	wp_enqueue_style('style');
	wp_enqueue_style('fonts');
	wp_enqueue_script('jquery');
	// wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), '4.0.0', true );
	// wp_enqueue_script('script', get_template_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true);
  }
  add_action('wp_enqueue_scripts', 'smart_styles');

  
function storefront_credit() {
	?>
	<div id="contacto">
		<img class="garantia" src="<?php echo get_template_directory_uri(); ?>/assets/images/garantia-min.png" alt="">
		<div class="info1">
			<div class="elemento">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/devoluciones.png" alt="">
				<strong>POLÍTICAS DE DEVOLUCIONES</strong>
				<p>
					Siempre con los mejores artículos y a precios muy accesibles al alcance de tu economía
				</p>
			</div>
			<div class="elemento">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/como comprar-min.png" alt="">
				<strong>COMO COMPRAR</strong>
				<p>
					Siempre con los mejores artículos y a precios muy accesibles al alcance de tu economía
				</p>
			</div>
			<div class="elemento">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/garantia de compra-min.png" alt="">
				<strong>GARANTÍA DE COMPRA</strong>
				<p>
					Siempre con los mejores artículos y a precios muy accesibles al alcance de tu economía
				</p>
			</div>
		</div>
		<div class="info_contacto">
			<h3>Contáctanos</h3>
			<div class="contenedor">
				<div class="enlaces">
					<div class="grupo">
						<a href="" class="enlace">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.png" alt="">
							Lorem ipsum dolor sit amet consectetur.
						</a>
						<a href="" class="enlace">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/g+.png" alt="">
							Lorem ipsum dolor sit amet consectetur.
						</a>
					</div>
					<div class="grupo">
						<a href="" class="enlace">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/insta.png" alt="">
							Lorem ipsum dolor sit amet consectetur.
						</a>
						<a href="" class="enlace">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/whats2.png" alt="">
							Lorem ipsum dolor sit amet consectetur.
						</a>
					</div>
				</div>
				<div class="logostarjetas">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/visa.png" alt="">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/master.png" alt="">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cyber.png" alt="">
				</div>
			</div>
		</div>
	</div>
    <div class="site-info">
	<div class="b-black t-white t-center" style="padding:0.5rem;">
      <span class="rights__text">Este sitio fue creado por <a href="https://techwebgt.com" class="text-white">TechWeb</a> - © <?php echo date('Y'); ?>
      </span>
    </div>
    </div><!-- .site-info -->
    <?php
}


//menu
function mis_menus() {
	register_nav_menus(
	  array(
		'principal' => __( 'Menu principal' ),
	  )
	);
  }
  add_action( 'init', 'mis_menus' );
  
  //crear clase para <a>
  add_filter( 'nav_menu_link_attributes', 'clase_menu', 10, 3 );
  
  function clase_menu($atts, $item, $args){
	$class = 'nav-link';
	$atts['class'] = $class;
	return $atts;
  }  